import sys
sys.path.append('../../')

from misc import Settings
from as_tools import utils

import numpy as np
import numpy.linalg as la
import os
import scipy.stats as stat


settings = Settings('settings')
os.system('mkdir -p %s' % settings.dir_tmp)

n = settings.n

# np.random.seed(0)
A_W = stat.ortho_group.rvs(dim=n)
A_L = np.diag(10**np.insert(np.linspace(0.1, 0.01,
                                        8, endpoint=False), [0, 1], [1, 0.6]))

A = np.dot(np.dot(A_W, A_L), A_W.T)
# A = np.diag(10**np.insert(np.linspace(0.1, 0.01,
#                                       8, endpoint=False), [0, 1], [1, 0.6]))

assert(np.allclose(np.dot(A_W, A_W.T), np.eye(n)))

np.savetxt('%s/A.txt' % settings.dir_tmp, A)

import model

x_true = settings.x_true

data = model.AbstractModel().instantiate(x_true).qoi()
np.savetxt(settings.data_file, [data])
