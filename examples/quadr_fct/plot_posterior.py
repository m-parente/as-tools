import env

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

sns.set()


def get_cmap(cmap):
    color_norm = mpl.colors.Normalize(vmin=-0.5, vmax=1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap=cmap)

    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color


cmap = get_cmap('OrRd')


settings = env.settings
dir_mcmc = settings.dir_mcmc
k = env.k


def _pairgrid_heatmap(x, y, **kws):
    cmap = sns.light_palette(kws.pop("color"), as_cmap=True)
    plt.hist2d(x, y, cmap=cmap, cmin=1, **kws)


samples = np.loadtxt('%s/x_samples_%id.txt' % (dir_mcmc, k))

choice = rnd.choice(len(samples), 1000)

corrcoeffs = np.corrcoef(samples.T)

x1_samples = samples[choice, 0]
x2_samples = samples[choice, 1]
x3_samples = samples[choice, 2]
x4_samples = samples[choice, 3]
x5_samples = samples[choice, 4]
x6_samples = samples[choice, 5]
x7_samples = samples[choice, 6]
x8_samples = samples[choice, 7]
x9_samples = samples[choice, 8]
x10_samples = samples[choice, 9]
df = pd.DataFrame(
    np.hstack((x1_samples[:, np.newaxis], x2_samples[:, np.newaxis],
               x3_samples[:, np.newaxis], x4_samples[:, np.newaxis],
               x5_samples[:, np.newaxis], x6_samples[:, np.newaxis],
               x7_samples[:, np.newaxis], x8_samples[:, np.newaxis],
               x9_samples[:, np.newaxis], x10_samples[:, np.newaxis])))  # ,
# columns=[r'$c$', r'$\alpha_{res}^l$', r'$\Delta\alpha_{res}$', r'$\dot{\lambda}^*$',
#          r'$m_\alpha$', r'$\beta^*$', r'$\lambda^*$', r'$m_\beta$'])
g = sns.PairGrid(df, height=0.7)
g = g.map_diag(sns.distplot, kde=False, bins=10)
g = g.map_lower(sns.kdeplot)

for i in range(env.n):
    for j in range(env.n):
        ax = g.axes[i, j]
        if j <= i:
            ax.set_xlim(-4, 4)
            ax.set_ylim(-4, 4)
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            ax.set_xlabel('')
            ax.set_ylabel('')
        else:
            # ax.set_visible(False)
            ax.set_facecolor('white')
            ax.text(0, 0,
                    r'$%.3f$' % np.round(corrcoeffs[j, i], decimals=3),
                    fontsize=10,
                    color=cmap(np.abs(corrcoeffs[j, i])),
                    ha='center', va='center')

plt.subplots_adjust(left=0.03, top=0.97, right=0.97,
                    bottom=0.03, hspace=0.1, wspace=0.1)

plt.show()
