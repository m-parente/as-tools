import env
import plotters
from as_tools import utils

import numpy as np
import numpy.linalg as la


x_true = env.settings.x_true
W1 = env.W1

y_true = np.dot(x_true, W1)

print y_true
exit()

settings = env.settings
A = np.loadtxt('%s/A.txt' % settings.dir_tmp)

eig_vals, eig_vecs = utils.get_eigenpairs(A)

print eig_vals
print eig_vecs
