import env

from as_tools import asm

import matplotlib.pyplot as plt
import numpy as np


k = 1
W1 = env.W[:, :k]

samples = env.prior_sample(size=100000)
y_samples = np.dot(samples, W1)

prior_y = asm.marginal_prior_y(samples, W1, kde_bandwidth=0.12, kde_kernel='gaussian')

ys = np.linspace(-3, 3)[:, np.newaxis]
plt.figure()
plt.hist(y_samples, bins=20, density=True)
plt.plot(ys, [prior_y(y) for y in ys])

plt.show()
