import env

from as_tools import asm
import plotters

import numpy as np
import numpy.random as rnd
from sklearn import metrics

import matplotlib.pyplot as plt


k = env.k

xs = env.xs
misfits = [env.misfitG(G) for G in env.Gs]

if k == 1:
    plotters.summary_plot(env.W1[:, 0], xs, misfits, poly_order=4,
                          with_fit=True)
elif k == 2:
    plotters.summary_plot_2d(
        env.W1[:, 0], env.W1[:, 1], xs, misfits, poly_order=4, with_fit=True)

plt.show()
