# as-tools

This is a collection of Python files for Bayesian inversion in active subspaces.

For using _calc_parallel.py_, the launcher script ([https://github.com/TACC/launcher](https://github.com/TACC/launcher)) is required.