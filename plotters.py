from as_tools import asm

import numpy as np
from sklearn import metrics

from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

sns.set()


def ploteig_vals(eig_vals, min_eig_vals, max_eig_vals, axes=None):
    n_eig_vals = len(eig_vals)

    if axes is None:
        fig = plt.figure()

    axes = plt.gca()

    axes.set_yscale('log')
    axes.margins(0.03)

    ran = range(1, n_eig_vals + 1)
    plt.plot(ran, eig_vals, 'o-', color='C0')
    plt.plot(ran, min_eig_vals, color='C0')
    plt.plot(ran, max_eig_vals, color='C0')
    plt.fill_between(ran, min_eig_vals, max_eig_vals, facecolor='C2')
    plt.xticks(range(1, n_eig_vals+1))

    axes.set_xlabel(r'Index $i$')
    axes.set_ylabel(r'Eigenvalue $\lambda_i$')

    plt.tight_layout()


def plot_active_subspace(eig_vals, eig_vecs, n_plot_eig_vals, min_eig_vals, max_eig_vals, min_subspace_errors, max_subspace_errors, mean_subspace_errors):
    """
    Plots eigenvalues, eigenvectors and subspace errors resulting from an active subspace computation.

    :param eig_vals: List of eigenvalues
    :param eig_vecs: Matrix with eigenvectors in columns
    :param integer n_plot_eig_vals: Number of eigenvalues to plot
    :param min_eig_vals: Bootstrap minimum of all eigenvalues
    :param max_eig_vals: Bootstrap maximum of all eigenvalues
    :param min_subspace_errors: Bootstrap minimum of subspace errors in all dimensions
    :param max_subspace_errors: Bootstrap maximum of subspace errors in all dimensions
    :param mean_subspace_errors: Bootstrap mean of subspace errors in all dimensions
    """
    eig_vals = eig_vals[0:n_plot_eig_vals]
    eig_vecs = eig_vecs[:, 0:n_plot_eig_vals]
    min_eig_vals = min_eig_vals[0:n_plot_eig_vals]
    max_eig_vals = max_eig_vals[0:n_plot_eig_vals]
    min_subspace_errors = min_subspace_errors[0:n_plot_eig_vals - 1]
    max_subspace_errors = max_subspace_errors[0:n_plot_eig_vals - 1]
    mean_subspace_errors = mean_subspace_errors[0:n_plot_eig_vals - 1]

    n_eig_vals = len(eig_vals)

    print "Eigenvalues: ", eig_vals
    print "Bootstrap minimum eigenvalues: ", min_eig_vals
    print "Bootstrap maximum eigenvalues: ", max_eig_vals
    print "Eigenvectors: ", eig_vecs
    print "Minimums of subspace errors: ", min_subspace_errors
    print "Maximums of subspace errors: ", max_subspace_errors
    print "Means of subspace errors: ", mean_subspace_errors

    # Plot eigenvalues -------------------------
    ploteig_vals(eig_vals, min_eig_vals, max_eig_vals)
    # ------------------------------------------

    # Plot eigenvectors ------------------------
    n = int(np.ceil(np.sqrt(n_eig_vals)))
    fig, axarr = plt.subplots(n, n)

    n_params = len(eig_vecs[:, 0])
    for i in range(n_eig_vals):
        eigVec = eig_vecs[:, i]

        coord = divmod(i, n)
        axes = axarr[coord[0], coord[1]]

        axes.bar(range(1, n_params + 1), eigVec)

        axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
        axes.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    fig.tight_layout()
    # ------------------------------------------

    # Plot subspace estimation errors-----------
    plt.figure()
    axes = plt.gca()

    axes.margins(0.03)
    axes.set_yscale('log')

    ran = range(1, n_eig_vals)
    plt.plot(ran, mean_subspace_errors, 'o-', color='C0')
    plt.plot(ran, min_subspace_errors, 'o-', color='C0')
    plt.plot(ran, max_subspace_errors, 'o-', color='C0')
    plt.fill_between(ran, min_subspace_errors, max_subspace_errors, facecolor='C1')

    axes.set_ylabel("Subspace distance")
    axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    plt.tight_layout()
    # -------------------------------------------

    plt.show()


def summary_plot(w, xs, values, poly_order=2, with_fit=False, plot_range=None):
    """
    Creates a 1D summary plot for a specified direction.

    :param w: Direction
    :param xs: List of locations
    :param values: List of function values
    :param boolean with_fit: Show a regression fit
    :param integer poly_order: Polynomial order of the regression fit (only active if with_fit is True)
    """
    wTx = np.dot(xs, w)

    fig = plt.figure()
    plt.scatter(wTx, values)
    plt.xlabel(r'$w_1^Tx$')
    plt.ylabel('Data misfit')

    if with_fit:
        resp_surf = asm.response_surface(xs, values, w, poly_order=poly_order)
        print "1D r2 score: %f" % metrics.r2_score(
            values, resp_surf(np.dot(xs, np.transpose([w]))))

        idx = wTx.argsort()
        plot_xs = wTx[idx] if plot_range is None else plot_range
        plt.plot(plot_xs, resp_surf(plot_xs[:, np.newaxis]), 'k')

    return fig


def summary_plot_2d(w1, w2, xs, values, with_fit=False, poly_order=2):
    """
    Creates a 2D summary plot for specified directions.

    :param w1: First direction
    :param w2: Second direction
    :param xs: List of locations
    :param values: List of function values
    :param boolean with_fit: Show a regression fit
    :param integer poly_order: Polynomial order of the regression fit (only active if with_fit is True)
    """
    w1Tx = np.dot(xs, w1)
    w2Tx = np.dot(xs, w2)

    fig = plt.figure()
    plt.scatter(w1Tx, w2Tx, c=values)
    plt.colorbar()

    if with_fit:
        resp_surf = asm.response_surface(
            xs, values, np.transpose([w1, w2]), poly_order=poly_order)
        print "2D r2 score: %f" % metrics.r2_score(
            values, resp_surf(np.dot(xs, np.transpose([w1, w2]))))

        # y1s = np.arange(np.min(w1Tx), np.max(w1Tx), step=0.05)
        # y2s = np.arange(np.min(w2Tx), np.max(w2Tx), step=0.05)

        # fig, ax = plotSurface(y1s, y2s, resp_surf, alpha=0.3)

        fig = plt.figure()
        ax = plt.gca(projection='3d')
        X = np.arange(np.min(w1Tx), np.max(w1Tx), step=0.05)
        Y = np.arange(np.min(w2Tx), np.max(w2Tx), step=0.05)

        X, Y = np.meshgrid(X, Y)

        res = np.reshape(resp_surf(np.concatenate(
            [zip(x, y) for x, y in zip(X, Y)])), np.shape(X))
        ax.plot_surface(X, Y, res, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False, alpha=0.3)

        colors = np.abs(np.subtract(values, [resp_surf([[x, y]])[0]
                                             for x, y in zip(w1Tx, w2Tx)]))
        scatter = ax.scatter(w1Tx, w2Tx, values, c=colors)

        ax.set_zlim(np.min(values), np.max(values))
        ax.set_xlabel(r'$w_1^Tx$')
        ax.set_ylabel(r'$w_2^Tx$')
        ax.set_zlabel('Data misfit')
        fig.colorbar(scatter)

    return fig, ax


def plot_samples(x, bins=20, normed=True):
    x_sh = np.shape(x)
    if len(x_sh) < 2:
        x = x[:, np.newaxis]
    n, dim = np.shape(x)

    for i in range(dim):
        plt.figure()
        plt.hist(x[:, i], bins, normed=normed)


def plot_contour(xs, ys, func, levels=None):
    X, Y = np.meshgrid(xs, ys)
    Z = np.reshape([func(np.array([x, y]))
                    for y in ys for x in xs], (len(xs), len(ys))).T
    plt.figure()
    plt.contour(X, Y, Z, levels=levels)
    plt.colorbar()


def plot_surface(xs, ys, func, alpha=1., ax=None):
    X, Y = np.meshgrid(xs, ys)
    Z = np.reshape([func(np.array([x, y]))
                    for y in ys for x in xs], (len(xs), len(ys))).T

    if ax is None:
        fig = plt.figure()
        ax = fig.gca(projection='3d')

    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False, alpha=alpha)
    fig.colorbar(surf)

    return fig, ax
