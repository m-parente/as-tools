import mcmc
from stats import regression
from utils import get_eigenpairs

import itertools as it
import numpy as np
import numpy.linalg as la
import numpy.random as np_rnd
import os
import random as rnd
import sklearn.neighbors as skl_nb

# np.seterr(all='raise')


def _compute_bootstrap_eigenpairs(grad_outer_samples, n_boot):
    M = len(grad_outer_samples)
    boot_eig_vals = [None] * n_boot
    boot_eig_vecs = [None] * n_boot

    for iBoot in range(0, n_boot):
        repl_samples = [rnd.choice(grad_outer_samples) for _ in range(M)]
        repl_C = np.sum(repl_samples, axis=0) / float(M)
        repl_eig_vals, repl_eig_vecs = get_eigenpairs(repl_C)
        boot_eig_vals[iBoot] = repl_eig_vals
        boot_eig_vecs[iBoot] = repl_eig_vecs

    return (np.array(boot_eig_vals), np.array(boot_eig_vecs))


def _compute_bootstrap_intervals(boot_eig_vals, boot_eig_vecs, eig_vecs):
    # Bootstrap interval for subspace errors
    num_eig_vals = boot_eig_vals.shape[1]
    n_boot = len(boot_eig_vecs)
    subspace_errors = np.empty([num_eig_vals - 1, n_boot])
    for i_dim in range(1, num_eig_vals):
        for iboot_eig_vecs in range(n_boot):
            repl_eig_vecs = boot_eig_vecs[iboot_eig_vecs]
            subspace_errors[i_dim - 1, iboot_eig_vecs] = la.norm(
                np.matmul(eig_vecs[:, 0:i_dim].T, repl_eig_vecs[:, i_dim:]), 2)

    return (np.min(boot_eig_vals, 0), np.max(boot_eig_vals, 0), np.min(subspace_errors, 1), np.max(subspace_errors, 1), np.mean(subspace_errors, 1))


def compute_data_misfit_gradient(Gs, jacGs, bay_inv_pb):
    """
    Computes the gradient of the data misfit for given QoI evaluations and Jacobians.

    :param Gs: List of QoI evaluations
    :param jacGs: List of QoI Jacobians
    :param bay_inv_pb: Object representing a Bayesian inverse problem
    """
    M = len(Gs)
    assert(M == len(jacGs))

    grad_outer_samples = [None] * M

    i = 0
    for G, jacG in zip(Gs, jacGs):
        jacF = bay_inv_pb.misfit_gradientG(G, jacG)

        grad_outer_samples[i] = np.outer(jacF.T, jacF)
        i += 1

    return np.array(grad_outer_samples, dtype=float)


def compute_active_subspace(grad_outer_samples, n_boot):
    """
    Computes the active subspace, i.e. it returns eigenvalues, eigenvectors and all bootstrap quantities.

    :param grad_outer_samples: List of Jacobian outer products
    :param integer n_boot: Number of re-computations for bootstrapping
    """
    C = assemble_C(grad_outer_samples)

    eig_vals, eig_vecs = get_eigenpairs(C)

    boot_eig_vals, boot_eig_vecs = _compute_bootstrap_eigenpairs(grad_outer_samples, n_boot)
    boot_eig_vals = np.insert(boot_eig_vals, 0, eig_vals, axis=0)

    min_eig_vals, max_eig_vals, min_subspace_errors, max_subspace_errors, mean_subspace_errors = _compute_bootstrap_intervals(
        boot_eig_vals, boot_eig_vecs, eig_vecs)

    return (eig_vals, eig_vecs, min_eig_vals, max_eig_vals, min_subspace_errors, max_subspace_errors, mean_subspace_errors)


def compute_active_subspace_from_samples(Gs, jacGs, bay_inv_pb, n_boot):
    """
    Runs the active subspace method with already computed samples.

    :param Gs: List with forward run samples
    :param jacGs: List with gradient samples
    :param bay_inv_pb: Corresponding Bayesian inverse problem
    :param integer n_boot: Number of re-computations for bootstrapping
    """
    grad_outer_samples = compute_data_misfit_gradient(Gs, jacGs, bay_inv_pb)

    return compute_active_subspace(grad_outer_samples, n_boot)


def assemble_C(grad_outer_samples):
    return np.sum(grad_outer_samples, axis=0) / float(len(grad_outer_samples))


def response_surface(xs, fs, W1, poly_order=2):
    """
    Constructs a response surface on a specified subspace.

    1. Computes y_i = W1^T*x_i samples.
    2. Finds polynomial regression fit g such that g(y_i)=f_i

    :param xs: List with points in the original space
    :param fs: List with function evaluations
    :param W1: Matrix whose range specifies the subspace (active variable)
    :param poly_order: Polynomial order for regression
    """
    ys = np.dot(xs, W1)
    return regression.polynomial_fit(ys, fs, order=poly_order)


def marginal_prior_y(prior_samples, W1, kde_bandwidth=0.05, kde_kernel='gaussian'):
    """
    Computes the marginal prior distribution on the active variable y.

    :param prior_samples: Prior samples
    :param W1: Matrix whose range specifies the subspace (active variable)
    :param kde_bandwidth: Bandwidth parameter for kernel density estimation
    :param kde_kernel: Kernel used for kernel density estimation (can be any kernel allowed by SciKit Learn)
    """
    ys = np.dot(prior_samples, W1)

    kde = skl_nb.KernelDensity(bandwidth=kde_bandwidth, kernel=kde_kernel).fit(ys)

    return lambda y: np.exp(kde.score_samples(y[np.newaxis, :] if len(np.shape(y)) <= 1 else y))


def mcmc_active_to_original_samples(active_samples, W1, W2, prior, proposal_sampler, z1, steps_per_active_sample, burn_in=50000, maxlag_inact=None, n_log=50):
    """
    Runs MCMC on the inactive variables (conditioned on active variable) to construct samples in the original space.

    :param active_samples: List with active samples
    :param W1: Matrix specifying active subspace
    :param W2: Matrix specifying inactive subspace
    :param prior: Function representing the prior on the original space
    :param proposal_sampler: Function producing MCMC proposals
    :param z1: Start value for every MCMC run
    :param integer steps_per_active_sample: Number of MCMC steps per active sample
    :param integer burn_in: Number of samples regarded as burn-in
    :param integer maxlag_inact: Number of autocorrelations taken into account for computing effective sample size of inactive samples
    :param n_log: Distance between two outputs of the acceptance rate
    """
    list_inactive_samples = [mcmc.mh_mcmc(lambda z: prior(np.dot(active_sample, W1.T) + np.dot(z, W2.T)),
                                          proposal_sampler, z1, steps_per_active_sample, n_log) for active_sample in active_samples]

    list_eff_inactive_samples = [mcmc.pick_eff_samples(
        inactive_samples, burn_in, maxlag=maxlag_inact) for inactive_samples in list_inactive_samples]

    lens = map(len, list_eff_inactive_samples)
    min_len = np.min(lens)
    list_eff_inactive_samples = map(lambda samples: samples[np_rnd.choice(
        len(samples), min_len)], list_eff_inactive_samples)

    return np.array([[(np.dot(active_sample, W1.T) + np.dot(inactive_sample, W2.T))
                      for inactive_sample in eff_inactive_samples]
                     for active_sample, eff_inactive_samples in zip(active_samples, list_eff_inactive_samples)])


def as_mcmc(W1, W2, reduced_misfit, proposal_sampler, prior_y, y1, steps=10**3, n_log=500):
    """
    Runs MCMC in the active subspace and returns also original samples if prior_cond_sampler is not None.

    :param W1: Matrix specifying active subspace
    :param W2: Matrix specifying inactive subspace
    :param reduced_misfit: Low-dimensional approximation of the data misfit function
    :param proposal_sampler: Function producing MCMC proposals
    :param prior_y: Function representing the prior on the active variable
    :param y1: Starting point for MCMC
    :param integer steps: Number of MCMC steps
    :param n_log: Distance between two log outputs
    """
    y_samples = np.empty((steps, len(y1)))
    yk = y1
    y_samples[0, :] = yk
    gyk = reduced_misfit(yk)
    k = 1
    accptd = 0

    while k < steps:
        y_ = proposal_sampler(yk)
        gy_ = reduced_misfit(y_)
        prior_y_ = prior_y(y_)

        accpt_ratio = np.min(
            [1, np.exp(gyk - gy_) * prior_y_ / prior_y([yk])]) if prior_y_ else 0

        if accpt_ratio >= np_rnd.uniform():
            yk = y_
            gyk = gy_
            accptd += 1

        y_samples[k, :] = yk

        if (k + 1) % n_log == 1:
            print "State %i: %s" % (k, `yk`)
            print "Acceptance rate at step %i: %s" % (
                k, "{0: .3}".format(accptd / float(k) * 100))

        k += 1

    return y_samples


def as_mcmc_with_response_surface(resp_surface, W1, W2, proposal_sampler, prior_y, y1, steps=10**3, n_log=50):
    return as_mcmc(W1, W2, resp_surface, proposal_sampler, prior_y, y1, steps, n_log)


def compute_activity_scores(eig_vals, eig_vecs):
    return np.dot(eig_vecs*eig_vecs, eig_vals)
