import numpy.linalg as la
import numpy.random as rnd
import numpy as np


def _multiQuad(r, width):
    return np.sqrt(r**2 + width**2)


def _invMultiQuad(r, width):
    return 1.0 / np.sqrt(r**2 + width**2)


def _gaussian(r, width):
    return np.exp(-r**2 / width**2)


def _dMultiQuad(r, width):
    return r / np.sqrt(r**2 + width**2)


def _dInvMultiQuad(r, width):
    return -r / (r**2 + width**2)**1.5


def _dGaussian(r, width):
    return -2 * r / width**2 * np.exp(-r**2 / width**2)


def _sampleSurfaceNSphere(dim, n=1, center=(0, 0), r=1):
    normals = rnd.normal(loc=0.0, scale=1.0, size=(n, dim))
    lengths = la.norm(normals, 2, axis=1).reshape(n, 1)

    return center + r * (normals / lengths)


def _getApproximationPairs(xs, ys, centers=[], widths=[], method='IMQ'):
    n = len(xs)
    m = len(centers)

    rbf = _invMultiQuad if method == 'IMQ' else \
        (_multiQuad if method == 'MQ' else
         (_gaussian if method == 'G' else
          None))

    gs = [lambda x, i=i: rbf(la.norm(x - centers[i]), widths[i])
          for i in range(m)]

    G_tr = [[gs[i_g](xs[i_x]) for i_x in range(n)] for i_g in range(m)]

    return la.lstsq(G_tr, ys)[0], gs


def approximate(xs, ys, centers=[], widths=[], method='IMQ'):
    """
    Approximates a function by radial basis functions.

    :param xs: List of locations
    :param ys: List of function values
    :param centers: List of basis' centers
    :param widths: List of basis' widths
    :param śtring method: Method to use (IMQ = inverse multiquadrics, MQ = multiquadrics, G = Gaussian)
    """
    if len(xs) != len(ys):
        raise Exception("xs and ys are of different length.")

    if len(centers) <= 0:
        centers = xs

    if np.isscalar(widths):
        widths = widths * np.ones(len(centers))

    if len(widths) <= 0:
        width = np.min([la.norm(xs[i] - xs[i + 1])
                        for i in range(len(xs) - 1)])
        widths = width * np.ones(len(centers))

    if len(centers) != len(widths):
        raise Exception("Centers and width are of different length.")

    n = len(xs)
    m = len(centers)

    ws, gs = _getApproximationPairs(xs, ys, centers, widths, method)

    return lambda x: np.sum([ws[i] * gs[i](x) for i in range(len(ws))])


def approximateGradientAt(x, f, widths=[], n=None, r=1e-3, method='IMQ'):
    """
    Approximates the gradient of a function by radial basis functions.

    :param x: Point of gradient evaluation
    :param f: Function to approximate the gradient for
    :param widths: List of basis' widths
    :param integer n: Number of cluster points
    :param float r: Radius of the ball the cluster points lay on
    :param string method: Method to use (IMQ = inverse multiquadrics, MQ = multiquadrics, G = Gaussian)
    """
    dimIn = len(x)

    if n is None:
        n = dimIn + 2

    xs = _sampleSurfaceNSphere(dimIn, n=n, center=x, r=r)
    n = len(xs)

    ys = np.array([f(x_) for x_ in xs])

    if len(ys.shape) <= 1:
        ys = ys.reshape(n, 1)

    dimOut = ys.shape[1]

    centers = xs

    if np.isscalar(widths):
        widths = widths * np.ones(len(centers))

    if len(widths) <= 0:
        widths = np.ones(len(centers))

    if len(centers) != len(widths):
        raise Exception("Centers and width are of different length.")

    m = len(centers)

    jac = [None] * dimOut

    for i_y in range(dimOut):
        ws, _ = _getApproximationPairs(xs, ys[:, i_y], centers, widths, method)

        drbf = _dInvMultiQuad if method == 'IMQ' else \
            (_dMultiQuad if method == 'MQ' else
             (_dGaussian if method == 'G' else
              None))

        jac[i_y] = [np.sum([ws[i] * (x[j] - centers[i][j]) / la.norm(x - centers[i]) * drbf(la.norm(x - centers[i]), widths[i])
                            for i in range(len(ws))])
                    for j in range(len(x))]

    return np.array(jac) if dimOut > 1 else np.array(jac[0])
