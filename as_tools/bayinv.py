import numpy as np
import numpy.linalg as la
import scipy.linalg as sla


class BayesianInverseProblem:
    """Class representing a Bayesian inverse problem"""

    def __init__(self, data, abs_model, inv_noise_cov):
        """
        Initializes a Bayesian inverse problem.

        :param data: Data
        :param abs_model: Abstract model (must implement an 'instantiate' method to get a concrete model for a specified parameter)
        :param inv_noise_cov: Inverse of covariance matrix of noise
        """
        self.data = data
        self.abs_model = abs_model
        self.inv_noise_cov = inv_noise_cov
        self._sqrt_inv_noise_cov = sla.sqrtm(inv_noise_cov)

        self.dimData = len(data)

    def misfit(self, x):
        """Computes the data misfit for a specified parameter."""
        m = self.abs_model.instantiate(x)
        return self.misfitG(m.qoi())

    def misfitG(self, G):
        return 0.5 * la.norm(np.dot(self._sqrt_inv_noise_cov, self.data - G))**2

    def misfit_gradient(self, x):
        m = self.abs_model.instantiate(x)
        return self.misfit_gradientG(m.qoi(), m.jac())

    def misfit_gradientG(self, G, jacG):
        return np.dot(np.dot(jacG.T, self.inv_noise_cov), self.data - G)
