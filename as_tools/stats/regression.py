import numpy as np
from sklearn import linear_model as lm
from sklearn import pipeline as pipe
from sklearn import preprocessing as pre_proc


def polynomial_fit(x, y, order=2):
    """
    Fits a polynomial to function values.

    :param x: List of locations
    :param y: List of function values
    :param integer order: Polynomial order
    """
    x = x[:, np.newaxis] if len(np.shape(x)) <= 1 else x

    model = pipe.Pipeline([('poly', pre_proc.PolynomialFeatures(order)),
                           ('linear', lm.LinearRegression())])
    model.fit(x, y)
    # print model.named_steps['poly'].powers_
    # print model.named_steps['linear'].coef_

    return lambda x_: model.predict(x_[np.newaxis, :] if len(np.shape(x_)) <= 1 else x_)


def linear_fit(x, y):
    """Fits a linear function to function values."""
    reg = lm.LinearRegression()
    reg.fit(x[:, np.newaxis], y)

    return reg


def quadratic_fit(x, y):
    """Fits a quadratic function to function values."""
    return polynomial_fit(x, y, order=2)
