import multiprocessing as mp
import numpy as np
import numpy.linalg as la
import os
import sys


def get_eigenpairs(C):
    """Computes eigenpairs of a matrix in decreasing order of eigenvalues."""
    eig_vals, eig_vecs = la.eigh(C)

    idx = eig_vals.argsort()[::-1]
    eig_vals = eig_vals[idx]
    eig_vecs = eig_vecs[:, idx]

    return (eig_vals, eig_vecs)


def save_active_subspace(eigVals, eigVecs, minEigVals, maxEigVals, minSubspaceErrors, maxSubspaceErrors, meanSubspaceErrors, dir, prefix=None):
    prefix = prefix + '_' if prefix is not None else ''

    np.savetxt('%s/%sasm_eigVals.txt' % (dir, prefix), eigVals)
    np.savetxt('%s/%sasm_eigVecs.txt' % (dir, prefix), eigVecs)
    np.savetxt('%s/%sasm_minEigVals.txt' % (dir, prefix), minEigVals)
    np.savetxt('%s/%sasm_maxEigVals.txt' % (dir, prefix), maxEigVals)
    np.savetxt('%s/%sasm_minSubspaceError.txt' %
               (dir, prefix), minSubspaceErrors)
    np.savetxt('%s/%sasm_maxSubspaceErrors.txt' %
               (dir, prefix), maxSubspaceErrors)
    np.savetxt('%s/%sasm_meanSubspaceErrors.txt' %
               (dir, prefix), meanSubspaceErrors)


def construct_G_jacG_parallel(samples, script_name, dir, cpu_count=None):
    np.savetxt('%s/samples.txt' % dir, samples)

    cmds = ''
    for i in range(len(samples)):
        sample = samples[i]
        cmd = sys.executable + " " + script_name + " " + \
            str(i) + " " + ' '.join(str(p) for p in sample)
        cmds += cmd + ' > /dev/null\n'

    launcherFile = '%s/asmLauncher_runs.txt' % dir

    with open(launcherFile, 'w') as file:
        file.write(cmds)

    os.environ["LAUNCHER_JOB_FILE"] = launcherFile
    os.environ["LAUNCHER_DIR"] = os.environ["SW_DIR"] + "/launcher"
    os.environ["LAUNCHER_PPN"] = str(cpu_count if cpu_count is not None else mp.cpu_count() - 1)

    os.system("bash $LAUNCHER_DIR/paramrun")
